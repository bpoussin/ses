package main

import (
    "crypto/aes"
    "crypto/cipher"
    "crypto/sha256"
    "errors"
    "flag"
    "fmt"
    "io/ioutil"
    "os"
    "github.com/giorgisio/goav/avformat"
)

// compute sha256 of file, return 32 bytes array
func getSHA256(filename string) ([]byte, error) {
    src io.Reader
     bs, err := ioutil.ReadFile("test1.txt")
     if err != nil {
           return result, err
     }
     h := sha256.New()

     buf := make([]byte, 64*1024)
     for {

         n, err := io.ReadFull(src, buf)
         if err != nil && err != io.ErrUnexpectedEOF && err != io.EOF {
             panic(err)
         }
         h.Write(buf[:n])

         if (n < len(buf)) {
             break;
         }
     }
 }

     result := h.Sum([]byte{})
     return result, nil
}

// hash must have length = 32 (sha256)
func enDeCrypt(encrypt bool, hash []byte, dst io.Writer, src io.Reader) error {

    key := hash[:16]
    iv := hash[16:]

    block, err := aes.NewCipher(key)
    if err != nil {
        panic(err)
    }

    var stream
    if encrypt {
        stream := cipher.NewCFBEncrypter(block, iv)
    } else {
        stream := cipher.NewCFBDecrypter(block, iv)
    }

    buf := make([]byte, 64*1024)
    for {

        n, err := io.ReadFull(src, buf)
        if err != nil && err != io.ErrUnexpectedEOF && err != io.EOF {
            return err
        }
        stream.XORKeyStream(buf, buf[:n])

        _, err = dst.Write(buf[:n])
        if err != nil {
            return err
        }

        if (n < len(buf)) {
            return nil;
        }
    }
}

func openInput(filename string) (result *avformat.Context, error) {
    if len(filename) == 0 || filename == "-" {
        filename = "pipe:"
    }

    if avformat.AvformatOpenInput(result, filename, nil, nil) != 0 {
        log.Println("Error: Couldn't open input file: ", filename)
        return nil, -1
	}

    if result.AvformatFindStreamInfo(nil) < 0 {
		log.Println("Error: Couldn't find stream information file: " filename)
		return nil, -2
	}

    result.AvDumpFormat(0, filename, 0)

    return result, 0
}

func openOutput(filename string, ifmt_ctx *avformat.Context) (ofmt_ctx *avformat.Context, error) {
    if len(filename) == 0 || filename == "-" {
        filename = "pipe:"
    }

    avformat.AvformatAllocOutputContext2(&ofmt_ctx, NULL, NULL, filename);
    ofmt := ofmt_ctx.Oformat();
    maxi := ifmt_ctx.NbStreams()
    streams := (*[1 << 30]C.team)(unsafe.Pointer(ifmt_ctx.Streams()))[:maxi:maxi]
    for i := 0; i < maxi; i++ {
        in_stream := streams[i];
        *codec := in_stream.Codec();
        out_stream := ofmt_ctx.AvformatNewStream(codec.codec);
        if !out_stream {
            log.Println("Failed allocating output stream");
            ret = AVERROR_UNKNOWN;
            return -1;
        }
        ret = out_stream.Codec().AvcodecCopyContext(codec);
        if (ret < 0) {
            log.Println("Failed to copy context from input to output stream codec context");
            return ret;
        }
        // out_stream.Codec()->codec_tag = 0;
        if ofmt_ctx.Oformat().Flags() & AVFMT_GLOBALHEADER {
            out_stream.Codec().Flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
        }
    }
    ofmt_ctx.AvDumpFormat(0, out_filename, 1);
    if !(ofmt.Flags() & AVFMT_NOFILE) {
        ret = avio_open(&(*ofmt_ctx)->pb, out_filename, AVIO_FLAG_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'", out_filename);
            return ret;
        }
    }
    ret = avformat_write_header(*ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
        return ret;
    }

    return 0;
}

}

func split(metaFilename, inFilename string) {
    fmt.Println("split:", metaFilename, inFilename)

    metaFile, err := os.Create(metaFilename)
    if err != nil {
        panic(err)
    }
    defer metaFile.Close()

    var inFile
    if len(inFilename) == 0 || inFilename == "-" {
        inFile = os.Stdin
    } else {
        inFile, err := os.Open(inFilename)
        if err != nil {
            panic(err)
        }
        defer inFile.Close()
    }

    avformat.AvRegisterAll()

}

func join(metaFilename, outFilename string) {
    fmt.Println("join:", metaFilename, outFilename)

    metaFile, err := os.Open(metaFilename)
    if err != nil {
        panic(err)
    }
    defer metaFile.Close()

    var outFile
    if len(outFilename) == 0 || outFilename == "-" {
        outFile = os.Stdout
    } else {
        outFile, err := os.Create(outFilename)
        if err != nil {
            panic(err)
        }
        defer outFile.Close()
    }

}

func main() {
    // Define flags
    splitAction := flag.Bool("s", false, "split action")
    joinAction := flag.Bool("j", false, "join action")

    // Parse
    flag.Parse()

    metaFilename := flag.Arg(0);
    inoutFile := flag.Arg(1);

    if _, err = os.Stat(metaFilename); os.IsNotExist(err) {
        splitAction = !joinAction;
    } else {
        joinAction = !splitAction
    }

    if splitAction {
        split(metaFilename, inoutFile)
    } else if joinAction {
        join(metaFilename, inoutFile)
    } else {
        flag.PrintDefaults()
    }

    // fmt.Println("Hello World ", 2 + "2 * 2.6")
    // err := errors.New("error message")
}
