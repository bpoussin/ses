/*
 * Copyright (c) 2013 Stefano Sabatini
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 * @file
 * libavformat/libavcodec demuxing and muxing API example.
 *
 * Remux streams from one container format to another.
 * @example remuxing.c
 */
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <libavutil/timestamp.h>
#include <libavformat/avformat.h>

static void log_packet(const AVFormatContext *fmt_ctx, const AVPacket *pkt, const char *tag)
{
    return;
    AVRational *time_base = &fmt_ctx->streams[pkt->stream_index]->time_base;
    printf("%s: pts:%s pts_time:%s dts:%s dts_time:%s duration:%s duration_time:%s stream_index:%d\n",
           tag,
           av_ts2str(pkt->pts), av_ts2timestr(pkt->pts, time_base),
           av_ts2str(pkt->dts), av_ts2timestr(pkt->dts, time_base),
           av_ts2str(pkt->duration), av_ts2timestr(pkt->duration, time_base),
           pkt->stream_index);
}

/**
 * Encrypt or decrypt, depending on flag 'should_encrypt'
 */
int en_de_crypt(int should_encrypt, char* in_file, char* out_file, char* hash) {
    FILE *ifp, *ofp;

    ifp = fopen(in_file, "rb"); //File to be encrypted; plain text
    if (!ifp) {
        fprintf(stderr, "can't open file source '%s' to encrypt\n", in_file);
        return -534;
     }

    ofp = fopen(out_file, "wb"); //File to be written; cipher text
    if (!ofp) {
        fclose(ifp);
        fprintf(stderr, "can't open file destination '%s' to encrypt\n", out_file);
        return -534;
    }

    unsigned char ckey[32], ivec[16];
    strncpy(ckey, hash, 32); // 256 bits
    strncpy(ivec, hash + 32, 16); // 128 bits


    const unsigned BUFSIZE=4096;
    unsigned char *read_buf = malloc(BUFSIZE);
    unsigned char *cipher_buf;
    unsigned blocksize;
    int out_len;
    EVP_CIPHER_CTX ctx;

    EVP_CipherInit(&ctx, EVP_aes_256_cbc(), ckey, ivec, should_encrypt);
    blocksize = EVP_CIPHER_CTX_block_size(&ctx);
    cipher_buf = malloc(BUFSIZE + blocksize);

    while (1) {

        // Read in data in blocks until EOF. Update the ciphering with each read.

        int numRead = fread(read_buf, sizeof(unsigned char), BUFSIZE, ifp);
        EVP_CipherUpdate(&ctx, cipher_buf, &out_len, read_buf, numRead);
        fwrite(cipher_buf, sizeof(unsigned char), out_len, ofp);
        if (numRead < BUFSIZE) { // EOF
            break;
        }
    }

    // Now cipher the final block and write it out.

    EVP_CipherFinal(&ctx, cipher_buf, &out_len);
    fwrite(cipher_buf, sizeof(unsigned char), out_len, ofp);

    // Free memory

    free(cipher_buf);
    free(read_buf);

    fclose(ifp);
    fclose(ofp);
}


void sha256_hash_string (unsigned char hash[SHA256_DIGEST_LENGTH], char outputBuffer[65]) {
    int i = 0;

    for(i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        sprintf(outputBuffer + (i * 2), "%02x", hash[i]);
    }

    outputBuffer[64] = 0;
}

int sha256_file(char *path, char outputBuffer[65]) {
    FILE *file = fopen(path, "rb");
    if (!file) {
        fprintf(stderr, "can't open file '%s' to compute sha256\n", path);
         return -534;
     }

    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    const int bufSize = 32768;
    unsigned char *buffer = malloc(sizeof(char) * bufSize);
    int bytesRead = 0;
    if(!buffer) {
        fprintf(stderr, "can't allocate memory to compute sha256 of '%s'\n", path);
        return ENOMEM;
    }
    while((bytesRead = fread(buffer, 1, bufSize, file)))
    {
        SHA256_Update(&sha256, buffer, bytesRead);
    }
    SHA256_Final(hash, &sha256);

    sha256_hash_string(hash, outputBuffer);
    fclose(file);
    free(buffer);

    return 0;
}

int openInput(AVFormatContext **ifmt_ctx, const char* in_filename) {
    // AVFormatContext *ifmt_ctx = *aifmt_ctx;

    if (avformat_open_input(ifmt_ctx, in_filename, 0, 0) < 0) {
        fprintf(stderr, "Could not open input file '%s'", in_filename);
        return -1;
    }
    if (avformat_find_stream_info(*ifmt_ctx, 0) < 0) {
        fprintf(stderr, "Failed to retrieve input stream information");
        return -1;
    }
    av_dump_format(*ifmt_ctx, 0, in_filename, 0);

    return 0;
}

int closeInput(AVFormatContext **ifmt_ctx) {
    avformat_close_input(ifmt_ctx);
    return 0;
}

int openOutput(AVFormatContext **ofmt_ctx, char* out_filename, AVFormatContext *ifmt_ctx) {
    char mpegts[] = "mpegts";
    char pipe[] = "pipe:";

    int ret, i;
    AVOutputFormat *ofmt = NULL;

    char *out = out_filename;
    char *format_name = NULL;

    if (strcmp(out_filename, "-") == 0) {
        format_name = mpegts;
        out = pipe;
    }

    avformat_alloc_output_context2(ofmt_ctx, NULL, NULL, out_filename);
    if (!*ofmt_ctx) {
        fprintf(stderr, "Could not create output context\n");
        ret = AVERROR_UNKNOWN;
        return ret;
    }
    ofmt = (*ofmt_ctx)->oformat;
    for (i = 0; i < ifmt_ctx->nb_streams; i++) {
        AVStream *in_stream = ifmt_ctx->streams[i];
        AVStream *out_stream = avformat_new_stream(*ofmt_ctx, in_stream->codec->codec);
        if (!out_stream) {
            fprintf(stderr, "Failed allocating output stream\n");
            ret = AVERROR_UNKNOWN;
            return -1;
        }
        ret = avcodec_copy_context(out_stream->codec, in_stream->codec);
        if (ret < 0) {
            fprintf(stderr, "Failed to copy context from input to output stream codec context\n");
            return ret;
        }
        out_stream->codec->codec_tag = 0;
        if ((*ofmt_ctx)->oformat->flags & AVFMT_GLOBALHEADER)
            out_stream->codec->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
    }
    av_dump_format(*ofmt_ctx, 0, out_filename, 1);
    if (!(ofmt->flags & AVFMT_NOFILE)) {
        ret = avio_open(&(*ofmt_ctx)->pb, out_filename, AVIO_FLAG_WRITE);
        if (ret < 0) {
            fprintf(stderr, "Could not open output file '%s'", out_filename);
            return ret;
        }
    }
    ret = avformat_write_header(*ofmt_ctx, NULL);
    if (ret < 0) {
        fprintf(stderr, "Error occurred when opening output file\n");
        return ret;
    }

    return 0;
}

int closeOutput(AVFormatContext **ofmt_ctx, int error) {
    AVOutputFormat *ofmt = NULL;

    if (error == 0) {
        av_write_trailer(*ofmt_ctx);
    }

    /* close output */
    ofmt = (*ofmt_ctx)->oformat;
    if (*ofmt_ctx && !(ofmt->flags & AVFMT_NOFILE))
        avio_closep(&(*ofmt_ctx)->pb);
    avformat_free_context(*ofmt_ctx);

    return 0;
}

int encrypt(FILE *meta_file, char* out_filename) {
    char hash[65], encrypt_hash[65], hash_sep[2] = "-\0", sep[2] = ";\0";

    sha256_file(out_filename, hash);
    printf("%s -> %s\n", out_filename, hash);

    char encrypt_out_filename[strlen(out_filename) + 10];
    sprintf(encrypt_out_filename, "encrypt-%s", out_filename);

    if (en_de_crypt(1, out_filename, encrypt_out_filename, hash)) {
        return -1;
    }

    sha256_file(encrypt_out_filename, encrypt_hash);
    printf("%s -> %s\n", encrypt_out_filename, encrypt_hash);


    fwrite(sep, sizeof(unsigned char), strlen(sep), meta_file);
    fwrite(encrypt_hash, sizeof(unsigned char), strlen(encrypt_hash), meta_file);
    fwrite(hash_sep, sizeof(unsigned char), strlen(hash_sep), meta_file);
    fwrite(hash, sizeof(unsigned char), strlen(hash), meta_file);

    return 0;
}

int split(const char *in_filename) {
    FILE *meta_file = NULL;
    AVOutputFormat *ofmt = NULL;
    AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx = NULL;
    AVPacket pkt;
    int ret, i, chunk = 0;

    char out_filename[strlen(in_filename) + 12];
    char meta_filename[strlen(in_filename) + 12];

    av_register_all();

    if ((ret = openInput(&ifmt_ctx, in_filename)) < 0) {
        fprintf(stderr, "Failed to open in file %s", in_filename);
        goto end;
    }

    sprintf(out_filename, "chunk-%.3u-%s", chunk, in_filename);
    if ((ret = openOutput(&ofmt_ctx, out_filename, ifmt_ctx)) < 0) {
        fprintf(stderr, "Failed to open out file %s", out_filename);
        goto end;
    }

    sprintf(meta_filename, "meta-%s.mse", in_filename);
    if ((meta_file = fopen(meta_filename, "wb")) == 0) {
        fprintf(stderr, "Failed to open meta file %s", meta_filename);
        goto end;
    }

    int stream_index = -1;
    long int next = 1;

    while (1) {
        AVStream *in_stream, *out_stream;
        ret = av_read_frame(ifmt_ctx, &pkt);
        if (ret < 0) {
            break;
        }

        // change output if necessary
        if ((stream_index < 0 || stream_index == pkt.stream_index) && pkt.dts != AV_NOPTS_VALUE && pkt.dts > next) {
            AVRational time_base = ifmt_ctx->streams[pkt.stream_index]->time_base;
            double freq = av_q2d(time_base);
            long int dts = pkt.dts * freq;
            if (dts % 600 < 1) {
                chunk++;
                stream_index = pkt.stream_index;
                next = pkt.dts + 500 / freq;
                closeOutput(&ofmt_ctx, 0);
                encrypt(meta_file, out_filename);

                sprintf(out_filename, "chunk-%.3u-%s", chunk, in_filename, &ifmt_ctx);
                fprintf(stdout, "DTS => %u -> %u -> %u (%u, %f) => %s\n", pkt.dts, dts, dts % 600, next, freq, out_filename);
                ret = openOutput(&ofmt_ctx, out_filename, ifmt_ctx);
                if (ret < 0) {
                    break;
                }
            }
        }

        in_stream  = ifmt_ctx->streams[pkt.stream_index];
        out_stream = ofmt_ctx->streams[pkt.stream_index];
        log_packet(ifmt_ctx, &pkt, "in");
        /* copy packet */
        pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
        pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
        pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
        pkt.pos = -1;
        log_packet(ofmt_ctx, &pkt, "out");
        ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
        if (ret < 0) {
            fprintf(stderr, "Error muxing packet\n");
            // break;
        }
        av_packet_unref(&pkt);
    }
end:
    /* close output */
    closeOutput(&ofmt_ctx, ret);
    encrypt(meta_file, out_filename);

    avformat_close_input(&ifmt_ctx);

    if (meta_file) {
        fclose(meta_file);
    }

    if (ret < 0 && ret != AVERROR_EOF) {
        fprintf(stderr, "Error occurred: %s\n", av_err2str(ret));
        return 1;
    }
    return 0;
}

int join(char *meta_filename, char *out_filename) {
    FILE *meta_file = fopen(meta_filename, "rb");
    const unsigned BUFSIZE=4096;
    unsigned char *read_buf = malloc(BUFSIZE);

    while (1) {
        int numRead = fread(read_buf, sizeof(unsigned char), BUFSIZE, ifp);
        if (numRead < BUFSIZE) { // EOF
            break;
        }
    }
}

int join_local(char *out_filename, int start, int end, char **in_filenames) {
    AVOutputFormat *ofmt = NULL;
    AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx = NULL;
    AVPacket pkt;

    int ret, i;
    char *in_filename;

    av_register_all();

    for (i = start; i < end; i++) {
        in_filename  = in_filenames[i];
        if ((ret = openInput(&ifmt_ctx, in_filename)) < 0) {
            fprintf(stderr, "Failed to open in file %s", in_filename);
            goto end;
        }

        if (ofmt_ctx == NULL) {
            if ((ret = openOutput(&ofmt_ctx, out_filename, ifmt_ctx)) < 0) {
                fprintf(stderr, "Failed to open out file %s", out_filename);
                goto end;
            }
        }

        while (1) {
            AVStream *in_stream, *out_stream;
            ret = av_read_frame(ifmt_ctx, &pkt);
            if (ret < 0) {
                break;
            }

            in_stream  = ifmt_ctx->streams[pkt.stream_index];
            out_stream = ofmt_ctx->streams[pkt.stream_index];
            log_packet(ifmt_ctx, &pkt, "in");
            /* copy packet */
            pkt.pts = av_rescale_q_rnd(pkt.pts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
            pkt.dts = av_rescale_q_rnd(pkt.dts, in_stream->time_base, out_stream->time_base, AV_ROUND_NEAR_INF|AV_ROUND_PASS_MINMAX);
            pkt.duration = av_rescale_q(pkt.duration, in_stream->time_base, out_stream->time_base);
            pkt.pos = -1;
            log_packet(ofmt_ctx, &pkt, "out");
            ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
            if (ret < 0) {
                fprintf(stderr, "Error muxing packet\n");
                // break;
            }
            av_packet_unref(&pkt);
        }

        closeInput(&ifmt_ctx);
    }
end:
    /* close output */
    closeOutput(&ofmt_ctx, ret);

    if (ret < 0 && ret != AVERROR_EOF) {
        fprintf(stderr, "Error occurred: %s\n", av_err2str(ret));
        return 1;
    }
    return 0;
}

void usage(char *progname) {
    printf("usage: %s -s input \n"
           "usage: %s -j output input* \n"
           "\toutput can be - to out to stdin \n"
           "\tinput list of file input \n"
           "\n", progname, progname);
}

int main(int argc, char **argv) {

    if (argc < 3) {
        usage(argv[0]);
        return 1;
    }

    if (strcmp("-j", argv[1]) == 0) {
        return join_local(argv[2], 3, argc, argv);
    } else if (strcmp("-s", argv[1]) == 0) {
        return split(argv[2]);
    } else {
        usage(argv[0]);
        return 1;
    }
}
