Besoin
======

Partager des fichiers avec d'autres utilisateurs.

Contraintes:
- partage via des partages publics
- personne ne doit pouvoir regarder le contenu s'il n'est pas autorisé
- partagé des données générées par différentes personnes dont le contenu est le
  même mais différent au niveau binaire

Le système est utilisable pour tous types de fichiers, mais nous ne prendrons
ici seulement l'exemple de l'enregistrement du broadcast d'une chaine de
télévision via un tunner dvb. Car dans les autres cas, les problèmes sont beaucoup
moins nombreux et donc la solution plus simple.

L'enregistrement prend de la place, il n'est pas légalement diffusable,
mais d'autres personnes ont peut-être enregistré le même programme quelque part
dans le monde. Il serait donc intéressant de mutualiser l'espace de stockage
avec toutes les personnes ayant enregistrer la même chose.

Les problèmes:
- chaque personne à sa version de l'enregistre, c'est à dire que chaque personne
  a enregistré en fonction de sa qualité de réception, donc chaque enregistrement
  bien que représentant la même chose, n'est peut-être pas identique au niveau
  binaire.
- pour enregistrer le même programme, deux personnes ont pû indiquer des temps
  de démarrage différents. Les deux enregistrements seront donc différents au
  niveau de la durée, et du binaire. Mais il faut tout de même réussir
  à mutualiser l'enregistrement.
- comment garantir que seules les personnes ayant réellement enregistré le
  programme pourront y accéder
- comment et ou partager les données d'enregistrements (comment garantir la non
  corruption des fichiers partagés (intentionnellement ou non intentionnellement,
  même par une personne ayant fait l'enregistrement))


Les études à mener:
- faire le même enregistrement via 2 tuners différent et comparer les fichiers
  trouver des points commun que seules les personnes ayant fait l'enregistrements
  peuvent connaitre.
- comment découper deux enregistrements du même programme pour que les fichiers
  générés représentent les mêmes moments même si les enregistrements ne débutent
  et ne finissent pas au même moment. (ex: 20h50 et 20h55)


Idée général d'implantation
===========================

- prendre un enregistrement
- le coupé en bout de N minutes en commençant toujours aux mêmes temps
  (exemple: par block de 10min en partant de 0, 10, 20, 30, 40, 50 minutes des heures)
  de cette façon, on garanti que deux enregistrement de plus de 20 minutes auront
  des bloques en commun s'ils représentent le même programme
- faire le sha256 (ou une signature) des blocks (pour permettre plus tard la
  vérification de validité)
- générer une clé unique pour chaque block, seules les personnes ayant fait
  l'enregistrement doit pouvoir générer cette clé (une première approche naïve
  peut-être de prendre le sha256 du block lui même. Cette approche n'est peut-être pas
  valide si deux tuner n'enregistrement pas la même chose (voir "étude à mener")).
- crypter le sha256 du block et le block avec cette clé
- générer le sha256 de la clé, et utiliser ce sha256 comme nom de fichiers
  pour le block. (chaque block représentant la même partie d'un programme quelque
  soit le tuner utilisé aura donc le même nom de fichier (sha256 de la clé que
  seules les personnes ayant fait l'enregistrement connaissent))
- générer un fichier meta contenant des informations générales (sur le programme,
  sur le format du fichier, ...) et la liste des couples clés/sha256(ou signature)
  de chaque block. Ce fichier doit rester secret.
- les blocks cryptés sont mis sur un espace public. Avec la signature de
  l'utilisateur l'ayant mis a disposition (voir problème ci-dessous).

Problèmes:
- un utilisateur mal veillant pourrait enregistrer un programme pour récupérer
  les clés de block, mais au lieu d'encoder le block, encoder autre chose. Il
  pourrait alors pousser vers l'espace de stockage public des fichiers corrompus
  Comment les détecter ? Comment ne pas les utiliser ?
  Au début de chaque block, on a le sha256 du block, on pourrait alors récupérer
  seulement le début du fichier le décrypter et voir s'il correspond ou non au
  sha256 de notre fichier meta. Si ce n'est pas le cas, on considère que ce n'est
  pas le même fichier ? Si l'étude des enregistrement montre que le même programme
  enregistrer avec une réception 'parfaite' par deux tuners différents ne donnne
  pas le même sha256, il faudra trouver un moyen de générer une signature de block
  qui permette de prendre en compte la variance de l'enregistrement pour des
  différences mineures.
- Il faut que plusieurs bloques portant le même nom (sha256) puissent exister
  en même temps dans l'espace public de stockage. Il faut donc que la personne
  qui à poussé ce block le 'signe' pour que le couple signature/block deviennent
  unique. Et qu'on puisse avoir la liste de tous les blocks portant un certain
  nom et choisir de télécharger tel ou tel block en fonction de la signature
  de la personne qui l'a uploadé. On pourrait alors mettre en place une réputation
  des signatures et récupérer les blocks en premier lieu des signature les plus
  réputées. (réputation automatique en fonction des blocks corrompus,
  mais aussi humaine en fonction de la qualité de l'enregistrement. Mais comment
  faire pour que l'utilisateur en visionnant son programme puisse faire un retour
  sur la qualité du block ? et surtout est-ce vraiment util ? il y aurait d'ailleurs
  deux réputation, celle du block (qualité) et celle de la signature (fake)).
  Les réputations, sont-elles plubliques ou privées (chaque utilisateur stocke
  la réputation des signatures dont il a récupérer les blocks.) Si on fait une
  réputation plublique, il faut prévoir la mailvaillance de quelqu'un.
  (création de signature uniquement pour mettre de mauvaise réputation à d'autre.)
  Il faudrait donc que pour pouvoir modifier une réputation publique avoir nous
  même bonne réputation et partager N block ? (il existe surement des travaux
  autour de la réputation). Par contre où et comment stocker ces réputations
  publiques sans serveur central.

Problème à résoudre:
- signature d'enregistrement et savoir que deux enregistrements sont quasi
  identique avec seulement cette signature.


Idée:
On suppose que la majorité des enregistrements sont correct, en tout cas qu'ils
sont les plus nombreux (ce qui devrait être le cas, car que plusieurs tuner
enregistre une mauvaise réception de la même façon est peut probable.)
Dans ce cas, lors qu'un block doit être soumis, le client regarde avant s'il
n'y a pas dejà un block pour la clé qu'il souhaite pousser. Si c'est le cas, au
lieu de faire un upload, il download le block et vérifie qu'il est identique au
sien. Si c'est le cas, il signe le block pour indiquer qu'il a été vérifié et qu'il
correspond au programme enregistrer. A partir d'un certain nombre de signatures
ou que dans la liste des signatures on retrouve une signature dont la réputation
nous convient, on ne ne fait pas la vérification et on accept ce block comme
block valide.
La vérification pourrait se faire par le sha256 des images I et des flux
associé (son, texte, ...) ?

L'idée serait que même quelqu'un avec une mauvaise réception, mais qui arrive
a avoir la bonne clé d'identification du block pourrait se retrouver avec un
enregistrement parfait par les autres qui ont aussi fait l'enregistremnt.

Bilan de la réflexion:
On souhaite garder/privilégier les enregistrements de bonne qualité. Donc le
SHA256 sur le fichier ou les frames est un bon moyen.


On a un fichier de plus ou moins bonne qualité, une clé unique pour ce fichier,
que seul les personnes ayant le fichier peuvent connaitre. Il faut lier cette
clé unique et secrète avec le fichier ayant la meilleur qualité (normalement
la version la plus nombreuse existante), et faire que cela ne soit garantie
et non remise en cause. Pour cela, nous pouvons peut-etre utilise les blockchains.
Le couple hash du hash de la cle + hash du block crypté sont ajouter au blockchain.
Lorsqu'un utilisateur souhaite récupérer un block, il doit avoir ça clé. Il en
fait le hash et recherche le hash dans le blockchain pour connaitre le block à
récupérer. Il récupère le block, vérifie son hash, le décrypt avec la clé.

Qui peut ajouter au blockchain ?
Lorsque quelqu'un souhaite pousser un block, son block doit d'accord apparaitre
dans le blockchain. Tant que ce n'est pas le cas, il n'a pas le droit de le mettre
sur l'espace de stockage public.
Il annonce donc le hash de son block crypté et le hash-hash de la clé, tout ceux
qui ont aussi enregistré ce block on aussi la clé, il peuvent donc vérifier le
que le hash est bien le même que le leur, et il y a consensus, ce consensus est
consigné dans le blockchain. S'il n'y a pas consensus, rien est écrit dans le  
blockchain et le block ne peut pas être poussé dans l'espace de stockage public.
L'utilisateur garde donc son block mais ne pourra pas le partager. Par contre,
s'il trouve dans le blockchain la cle de son block avec un autre hash de block,
il peut vérifier de façon moins strict que c'est le même contenu (au parasite de
réception près). Si c'est bien le même contenu, alors il accepte le consensus
du blockchain et supprime son propre block pour le remplacer par celui du blockchain.
Si ce n'est pas le même contenu, il continu a garder sa propre version.

Besoin:
- trouver une clé qui identifie un block de façon unique, qui ne soit pas dépendant
  de la qualité de réception du tuner, mais qui ne soit connu que des personnes
  ayant vu passer l'info via le tuner. (si cette clé n'existe pas on devra
  se rabattre sur le hash du block et donc les personnes ayant eu une mauvaise
  réception ne pourront pas bénéficier d'une meilleur qualité provenant des autres)
- trouver un moyen de comparer deux enregistrements de qualité différente, et
  déterminer s'ils contiennent la même information ou non.
- trouver un moyen de stockage distribuer qui n'accepte de partager que des blocks
  contenu dans le blockchain.
- trouver les algo de consensus réparti

En attendant tout ça, implantation plus simple:
- découpe en bout de 10min (ffmpeg)
- cryptage (aes)
- création du fichier méta
- partage des bouts cryptés via bittorrent sans fichier torrent (juste la DHT)
- normalement les personnes ayant une bonne réception auront le même block
  crypté, et seront les plus nombreux
- lorsqu'un utilisateur manque de place (70% d'occupation disque), il regarde
  les blocks qui ont le plus de copie disponible, et le supprime localement (si
  égalité, alors supprime le plus vieux)
- si l'utilisateur a demandé à garder explicitement un enregistrement et que
  celui-ci arrive à moins de N copie, alors on le récupère en local


Test d'enregistrement avec deux tuner sur deux sites distincts
==============================================================

L'image des deux enregistrement est bon, le découpage est fait au même
moment, mais les sha256 sont différent. On ne peut donc pas se baser sur le
sha256 du flux pour mutualiser les enregistrements. Il faut donc trouver une
clé unique que seules les personnes ayant eu le flux peuvent créer. Il faut
aussi trouver un moyen d'élire le flux qui sera associé avec la clé, et garantir
que ce flux n'a pas été corrompu volontairement ou non. Pour évité que les
personnes l'utilisant pour regarder leur enregistrement ne tombent sur autre
chose que leur programme.

Clé unique d'un block
=====================
La clé unique d'un block pourrait être le sha256 de la concaténation de
plusieurs informations du flux:
- nombre de streams (video, audio, text)
- SID, TSID, ONID du streams
- temps de début et de fin de block suivant le DTS
- autre encore plus discréminant à trouver
