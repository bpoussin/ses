go: src/go/ses.go
	go run src/go/ses.go

ses: src/c/ses.c
	gcc -o ses src/c/ses.c -lavutil -lavformat -lavcodec -lz -lavutil -lm -lcrypto 

clean:
	rm -f *~ src/c/*~ ses
